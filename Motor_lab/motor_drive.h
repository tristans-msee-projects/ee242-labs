// motor_drive.h

#include <stdint.h>
#include <stdbool.h>

#define	EN		16
#define	DIR		20
#define	STEP	21
#define	MS1		23
#define	MS2		24

//function prototypes
void motor_init (void);
void motor_step (uint32_t num_steps, bool dir);
void disable_motor(void);
void motor_pwm_drive (void);

