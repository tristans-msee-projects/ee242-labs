//I2C_devices.h

#include <stdlib.h>

typedef struct{
	int x;
	int y;
	int z;
} compass_data_S;

void init_devices(void);
void shutdown_devices(void);
float read_ADC(void);
compass_data_S read_compass(void);


