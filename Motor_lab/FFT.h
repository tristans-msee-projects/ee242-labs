/****************************************************************************************
* Program is modified from Harry Li's fft.c code
****************************************************************************************/

#ifndef FFT_H_
#define FFT_H_


struct Complex
{	double a;        //Real Part
	double b;        //Imaginary Part
};

void FFT(struct Complex *X, int M);


#endif /* FFT_H_ */
