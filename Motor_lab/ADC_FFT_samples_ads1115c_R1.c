// asd1115.c sample and perform FFT/Power spectrum
// operates in continuous mode

// code based on example from:
// by Lewis Loflin lewis@bvu.net
// www.bristolwatch.com
// http://www.bristolwatch.com/rpi/ads1115.html

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>    // read/write usleep
#include <stdlib.h>    // exit function
#include <inttypes.h>  // uint8_t, etc
#include <linux/i2c-dev.h> // I2C bus definitions
#include <sys/ioctl.h>
#include <math.h>
#include "FFT.h"
#include "ADC_comp.h"


// Connect ADDR to GRD.
// Setup to use ADC0 single ended

#define M 12
#define num_samples 1U << M

int fd;
FILE* data_file;
FILE* fft_data_file;
FILE* pow_spectrum_file;
// Note PCF8591 defaults to 0x48!
int asd_address = 0x48;
int16_t val;
uint8_t writeBuf[3];
uint8_t readBuf[2];
float myfloat;
float sample_time;
struct Complex data[num_samples +1] = { 0 };
uint16_t adc_data[num_samples];
double pow_data[num_samples];



/*
The resolution of the ADC in single ended mode 
we have 15 bit rather than 16 bit resolution, 
the 16th bit being the sign of the differential 
reading.
*/

int main() {

  // open device on /dev/i2c-1 the default on Raspberry Pi B
  if ((fd = open("/dev/i2c-1", O_RDWR)) < 0) {
    printf("Error: Couldn't open device! %d\n", fd);
    exit (1);
  }

  // connect to ADS1115 as i2c slave
  if (ioctl(fd, I2C_SLAVE, asd_address) < 0) {
    printf("Error: Couldn't find device on address!\n");
    exit (1);
  }

  // set config register and start conversion
  // AIN0 and GND, 4.096v, 860s/s
  // Refer to page 19 area of spec sheet
  writeBuf[0] = 1; // config register is 1
  writeBuf[1] = 0b11000010; // 0xC2 single shot off
  // bit 15 flag bit for single shot not used here
  // Bits 14-12 input selection:
  // 100 ANC0; 101 ANC1; 110 ANC2; 111 ANC3
  // Bits 11-9 Amp gain. Default to 010 here 001 P19
  // Bit 8 Operational mode of the ADS1115.
  // 0 : Continuous conversion mode
  // 1 : Power-down single-shot mode (default)

  writeBuf[2] = 0b11100101; // bits 7-0  0xE5
  // Bits 7-5 data rate default to 0b111 for 860SPS
  // Bits 4-0  comparator functions see spec sheet.

  // begin conversion
  if (write(fd, writeBuf, 3) != 3) {
    perror("Write to register 1");
    exit (1);
  }

  sleep(1);

  printf("ADC_FFT_samples will take %i readings.\n", num_samples);

 
  // set pointer to 0
  readBuf[0] = 0;
  if (write(fd, readBuf, 1) != 1) {
    perror("Write register select");
    exit(-1);
  }

  int count = 1;
  
  // open data file in ./voltage_data.csv
  if ((data_file = fopen("./voltage_data.csv", "w")) < 0) {
    printf("Error: Couldn't open data file! \n");
    exit (1);
  }
  
  
  while (1)   {

    // read conversion register
    if (read(fd, readBuf, 2) != 2) {
      perror("Read conversion");
      exit(-1);
    }

    // could also multiply by 256 then add readBuf[1]
    val = readBuf[0] << 8 | readBuf[1];

    // with +- LSB sometimes generates very low neg number.
    if (val < 0)   val = 0;
    
    adc_data[count-1] = val;
     
    usleep(1000000/860);

    count++; // inc count
    if (count == num_samples)   break;

  } // end while loop
  
  data[0].a = data[0].b = 0.0;
  int i;
  for (i=1; i<=num_samples; i++)
  {
    data[i].a = ADC_comp_get_voltage(adc_data[i-1]);
    data[i].b = 0.0;
    fprintf(data_file, "%f\n", data[i].a );
  }
  
  fclose(data_file);
  
  FFT(data, M);
  // open fft_data file in ./fft_data.csv
  if ((fft_data_file = fopen("./fft_data.csv", "w")) < 0) {
    printf("Error: Couldn't open fft data file!\n");
    exit (1);
  }
  count = 1;
  while(count <= num_samples){
    fprintf(fft_data_file, "%f,%f\n", data[count].a, data[count].b);
    count++;
  }
  fclose(fft_data_file);
  
  
    // open fft_data file in ./fft_data.csv
  if ((pow_spectrum_file = fopen("./pow_spectrum.csv", "w")) < 0) {
    printf("Error: Couldn't open power spectrum data file!\n");
    exit (1);
  }
  
  for (i=1; i<=num_samples; i++)
  {
    pow_data[i] = sqrt(pow(data[i].a,2) + pow(data[i].b,2));
    fprintf(pow_spectrum_file, "%f\n", pow_data[i]);
  }
    
  // power down ASD1115
  writeBuf[0] = 1;    // config register is 1
  writeBuf[1] = 0b11000011; // bit 15-8 0xC3 single shot on
  writeBuf[2] = 0b10000101; // bits 7-0  0x85
  
  if (write(fd, writeBuf, 3) != 3) {
    perror("Write to register 1");
    exit (1);
  }

  close(fd);

  return 0;
}
