// motor_drive.c

#include "motor_drive.h"
#include <stdio.h>
#include <wiringPi.h>
#include <softPwm.h>


void motor_init (void){
  wiringPiSetupGpio ();
  
  pinMode (EN, OUTPUT);
  digitalWrite(EN, HIGH);
  
  pinMode (DIR, OUTPUT);
  digitalWrite(DIR, HIGH);
  
  pinMode (MS1, OUTPUT);
  pinMode (MS2, OUTPUT);
  digitalWrite(MS1, HIGH);
  digitalWrite(MS2, HIGH);
}

void motor_step (uint32_t num_steps, bool dir){
  wiringPiSetupGpio ();
  pinMode (STEP, OUTPUT);
  digitalWrite(EN, LOW);
  digitalWrite(DIR, dir);
  delay(1);
  for (uint32_t i = 0; i<num_steps; i++){
    digitalWrite(STEP, HIGH);
    delay(1);
    digitalWrite(STEP, LOW);
    delay(1);
  }
  //digitalWrite(EN, HIGH);
}

void disable_motor(void){
  digitalWrite(EN, HIGH);
}

void motor_pwm_drive (void){
  printf ("PWM Motor driver\n") ;
  digitalWrite(EN, LOW);
  softPwmCreate(STEP, 1, 2);
  delay(5000); //ms
  digitalWrite(EN, HIGH);
}
