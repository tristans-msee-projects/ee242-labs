#include <stdio.h>
#include "motor_drive.h"
#include "I2C_devices.h"
#include <wiringPi.h>
#include <math.h>
#include <stdbool.h>

#define PI            3.14159265
#define RANGE_PULSES  1600


int main (void)
{
  init_devices();
  motor_init();
 
  
  int16_t adc_count = ((int16_t)(read_ADC() * RANGE_PULSES / 3.3)) - RANGE_PULSES/2;
  int16_t step_count = adc_count;
  bool print_heading = true;
  uint8_t print_count = 0;
  motor_step(0, true);
  
  for (int index = 0; index < 10000; index++) {
    adc_count = ((int16_t)(read_ADC() * RANGE_PULSES / 3.3)) - RANGE_PULSES/2;
    
    if (adc_count > step_count + 2) {
      motor_step(1, false);
      step_count++;
      print_heading = true;
      print_count = 0;
    } else if (adc_count < step_count - 2) {
      motor_step(1, true);
      step_count--;
      print_heading = true;
      print_count = 0;
    } else {
      if (print_heading && (print_count > 20)) {
        compass_data_S compass_reading = read_compass();
        float heading = (atan2(compass_reading.y, compass_reading.x) * 180)/PI;
        if (heading < 0){
          heading += 360;
        }
        printf("Compass heading: %f    ADC Voltage: %f\n", heading, read_ADC());
        print_heading = false;
      } else if (print_heading){
        print_count++;
      }
    }
    delay(2);
  }
  
  disable_motor();
  
  //motor_step(5, true);
  
  //motor_pwm_drive();
 

  return 0 ;
}


