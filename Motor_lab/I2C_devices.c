//I2C_devices.c
//read i2c based ADC (ads1115) and compass (lsm303)

// code based on example from:
// by Lewis Loflin lewis@bvu.net
// www.bristolwatch.com
// http://www.bristolwatch.com/rpi/ads1115.html

#include "I2C_devices.h"
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>    // read/write usleep
#include <stdlib.h>    // exit function
#include <inttypes.h>  // uint8_t, etc
#include <linux/i2c-dev.h> // I2C bus definitions
#include <sys/ioctl.h>
#include <math.h>
#include "ADC_comp.h"


// Connect ADDR to GRD.
// Setup to use ADC0 single ended

#define M 12
#define num_samples 1U << M

int fd;
FILE* data_file;
FILE* fft_data_file;
FILE* pow_spectrum_file;
// Note PCF8591 defaults to 0x48!
int asd_address = 0x48;
int lsm_address = 0x1E;
int16_t val;
uint8_t writeBuf[4];
uint8_t readBuf[2];
float myfloat;
float sample_time;
uint16_t adc_data[num_samples];
double pow_data[num_samples];



void init_devices(void) {

/*******************
 * ADS1115
 ******************/

  // open device on /dev/i2c-1 the default on Raspberry Pi B
  if ((fd = open("/dev/i2c-1", O_RDWR)) < 0) {
    printf("Error: Couldn't open device! %d\n", fd);
    exit (1);
  }

  // connect to ADS1115 as i2c slave
  if (ioctl(fd, I2C_SLAVE, asd_address) < 0) {
    printf("Error: Couldn't find device on address!\n");
    exit (1);
  }

  // set config register and start conversion
  // AIN0 and GND, 4.096v, 860s/s
  // Refer to page 19 area of spec sheet
  writeBuf[0] = 1; // config register is 1
  writeBuf[1] = 0b11000010; // 0xC2 single shot off
  // bit 15 flag bit for single shot not used here
  // Bits 14-12 input selection:
  // 100 ANC0; 101 ANC1; 110 ANC2; 111 ANC3
  // Bits 11-9 Amp gain. Default to 010 here 001 P19
  // Bit 8 Operational mode of the ADS1115.
  // 0 : Continuous conversion mode
  // 1 : Power-down single-shot mode (default)

  writeBuf[2] = 0b11100101; // bits 7-0  0xE5
  // Bits 7-5 data rate default to 0b111 for 860SPS
  // Bits 4-0  comparator functions see spec sheet.

  // begin conversion
  if (write(fd, writeBuf, 3) != 3) {
    perror("Write to register 1");
    exit (1);
  }
 
  usleep(50*1000);

  close(fd);
  
  
  
/*******************
 * LSM303
 ******************/
  
  // open device on /dev/i2c-1 the default on Raspberry Pi B
  if ((fd = open("/dev/i2c-1", O_RDWR)) < 0) {
    printf("Error: Couldn't open device! %d\n", fd);
    exit (1);
  }

  // connect to lsm303 as i2c slave
  if (ioctl(fd, I2C_SLAVE, lsm_address) < 0) {
    printf("Error: Couldn't find device on address!\n");
    exit (1);
  }
  
  
  // set CRA_REG_M register and start reading
  writeBuf[0] = 0x0; // CRA_REG_M register is 0x0
  writeBuf[1] = 0b10011100;
  // bits 7: temp sensor enable (0 -> enabled)
  // bits 0,1,5,6: must be zero
  // Bits 2-4: data rate (0b111 -> 220Hz)
  writeBuf[2] = 0b11100000; // CRB_REG_M
  // bits 5-7: gain setting (+/- 1.3Gauss)
  writeBuf[3] = 0b00000000; //MR_REG_M
  // bits 0-1: set continuous conversion

  // begin reading mag.
  if (write(fd, writeBuf, 4) != 4) {
    perror("Write to register 0x0");
    exit (1);
  }
  
  usleep(50*1000);
  close(fd);
  
}

void shutdown_devices(void){
/*******************
 * ADS1115
 ******************/

  // open device on /dev/i2c-1 the default on Raspberry Pi B
  if ((fd = open("/dev/i2c-1", O_RDWR)) < 0) {
    printf("Error: Couldn't open device! %d\n", fd);
    exit (1);
  }

  // connect to ADS1115 as i2c slave
  if (ioctl(fd, I2C_SLAVE, asd_address) < 0) {
    printf("Error: Couldn't find device on address!\n");
    exit (1);
  }

  writeBuf[0] = 1;    // config register is 1
  writeBuf[1] = 0b11000011; // bit 15-8 0xC3 single shot on
  writeBuf[2] = 0b10000101; // bits 7-0  0x85
  
  if (write(fd, writeBuf, 3) != 3) {
    perror("Write to register 1");
    exit (1);
  }
  close(fd);
  
/*******************
 * LSM303
 ******************/
  
  // open device on /dev/i2c-1 the default on Raspberry Pi B
  if ((fd = open("/dev/i2c-1", O_RDWR)) < 0) {
    printf("Error: Couldn't open device! %d\n", fd);
    exit (1);
  }

  // connect to lsm303 as i2c slave
  if (ioctl(fd, I2C_SLAVE, lsm_address) < 0) {
    printf("Error: Couldn't find device on address!\n");
    exit (1);
  }
  
  writeBuf[0] = 0x0;    // config register is 0x20
  writeBuf[1] = 0b00000111;
  
  if (write(fd, writeBuf, 2) != 2) {
    perror("Write to register 0x20");
    exit (1);
  }
  close(fd);
  
} 
  
  
float read_ADC(void)  {

  // open device on /dev/i2c-1 the default on Raspberry Pi B
  if ((fd = open("/dev/i2c-1", O_RDWR)) < 0) {
    printf("Error: Couldn't open device! %d\n", fd);
    exit (1);
  }

  // connect to ADS1115 as i2c slave
  if (ioctl(fd, I2C_SLAVE, asd_address) < 0) {
    printf("Error: Couldn't find device on address!\n");
    exit (1);
  }

  // set pointer to 0
  readBuf[0] = 0;
  if (write(fd, readBuf, 1) != 1) {
    perror("Write ADC register select");
    exit(-1);
  }

  // read conversion register
  if (read(fd, readBuf, 2) != 2) {
    perror("Read conversion");
    exit(-1);
  }

  // could also multiply by 256 then add readBuf[1]
  val = readBuf[0] << 8 | readBuf[1];

  // with +- LSB sometimes generates very low neg number.
  if (val < 0)   val = 0;
     
  close(fd);
  
  return ADC_comp_get_voltage(val);
}


compass_data_S read_compass(void){
  
  // open device on /dev/i2c-1 the default on Raspberry Pi B
  if ((fd = open("/dev/i2c-1", O_RDWR)) < 0) {
    printf("Error: Couldn't open device! %d\n", fd);
    exit (1);
  }

  // connect to lsm303 as i2c slave
  if (ioctl(fd, I2C_SLAVE, lsm_address) < 0) {
    printf("Error: Couldn't find device on address!\n");
    exit (1);
  }
  
  char reg[1] = {0};
  char data[1] = {0};
  // Read 6 bytes of data
	// msb first
	// Read xMag msb data from register(0x03)
	reg[0] = 0x03;
	write(fd, reg, 1);
	read(fd, data, 1);
	char data1_0 = data[0];

	// Read xMag lsb data from register(0x04)
	reg[0] = 0x04;
	write(fd, reg, 1);
	read(fd, data, 1);
	char data1_1 = data[0];

	// Read yMag msb data from register(0x05)
	reg[0] = 0x05;
	write(fd, reg, 1);
	read(fd, data, 1);
	char data1_2 = data[0];

	// Read yMag lsb data from register(0x06)
	reg[0] = 0x06;
	write(fd, reg, 1);
	read(fd, data, 1);
	char data1_3 = data[0];

	// Read zMag msb data from register(0x07)
	reg[0] = 0x07;
	write(fd, reg, 1);
	read(fd, data, 1);
	char data1_4 = data[0];

	// Read zMag lsb data from register(0x08)
	reg[0] = 0x08;
	write(fd, reg, 1);
	read(fd, data, 1);
	char data1_5 = data[0];

	// Convert the data
	int xMag = (data1_0 * 256 + data1_1);
	if(xMag > 32767)
	{
		xMag -= 65536;
	}	

	int yMag = (data1_4 * 256 + data1_5) ;
	if(yMag > 32767)
	{
		yMag -= 65536;
	}

	int zMag = (data1_2 * 256 + data1_3) ;
	if(zMag > 32767)
	{
		zMag -= 65536;
	}

  compass_data_S compass_reading = {0};
  compass_reading.x = xMag;
  compass_reading.y = yMag;
  compass_reading.z = zMag;
  
  close(fd);
  
  return compass_reading;
  
  
/*
  // open device on /dev/i2c-1 the default on Raspberry Pi B
  if ((fd = open("/dev/i2c-1", O_RDWR)) < 0) {
    printf("Error: Couldn't open device! %d\n", fd);
    exit (1);
  }

  // connect to lsm303 as i2c slave
  if (ioctl(fd, I2C_SLAVE, lsm_address) < 0) {
    printf("Error: Couldn't find device on address!\n");
    exit (1);
  }
  
  // set pointer to 3 (out x msB)
  readBuf[0] = 0x03;
  if (write(fd, readBuf, 1) != 1) {
    perror("Write register select");
    exit(-1);
  }
  
  // read conversion register
  if (read(fd, readBuf, 2) != 2) {
    perror("Read conversion");
    exit(-1);
  }

  close(fd);

  // could also multiply by 256 then add readBuf[1]
  int16_t heading = readBuf[0] << 8 | readBuf[1];

  return heading;
*/
}
  


