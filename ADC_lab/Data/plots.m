% EE 242 ADC lab
% Tristan French
close all
clear
clc

voltage = csvread('voltage_data.csv');
plot(voltage)
title('Voltage input signal')
xlabel('Samples')
ylabel('Voltage')

figure
power_spectrum = csvread('pow_spectrum.csv');
stem(power_spectrum)
title('Power spectrum')

figure
%power_spectrum = csvread('pow_spectrum.csv');
stem(power_spectrum)
set(gca, 'yscal', 'log')
title('Power spectrum (semi-log scale)')

M = 12;
N = 2^M;
F = (N/2 - 1);
m0 = round(F * .05)
S_m0 = sum(power_spectrum(1:m0))
S_t = sum(power_spectrum(1:F))

eta = S_m0/S_t


