#include "ADC_comp.h"
#include <stdint.h>

static const float slope[9] = { 
	8027.23,
	8018.24,
	8034.57,
	8007.75,
	8039.33,
	8035.90,
	8030.39,
	8006.21,
	8000.00,
};

static const float intercept[9] = { 
	0,
	3.63,
	-8.34,
	21.40,
	-25.83,
	-19.48,
	-7.13,
	55.83,
	74.00,
};




float ADC_comp_get_voltage(int16_t ADC_val){
	float voltage = -2;
	switch(ADC_val){
		case 0 ... 3242:
			voltage = ADC_val/slope[0] - (intercept[0])/(slope[0]);
			break;
		case 3243 ... 5880:
			voltage = ADC_val/slope[1] - intercept[1]/slope[1];
			break;
		case 5881 ... 8901:
			voltage = ADC_val/slope[2] - intercept[2]/slope[2];
			break;
		case 8902 ... 12000:
			voltage = ADC_val/slope[3] - intercept[3]/slope[3];
			break;
		case 12001 ... 14862:
			voltage = ADC_val/slope[4] - intercept[4]/slope[4];
			break;
		case 14863 ... 17996:
			voltage = ADC_val/slope[5] - intercept[5]/slope[5];
			break;
		case 17997 ... 20903:
			voltage = ADC_val/slope[6] - intercept[6]/slope[6];
			break;
		case 20904 ... 23481:
			voltage = ADC_val/slope[7] - intercept[7]/slope[7];
			break;
		case 23482 ... 32767:
			voltage = ADC_val/slope[8] - intercept[8]/slope[8];
			break;
		default:
			voltage = -1;
			break;
	}
	return voltage;
}
